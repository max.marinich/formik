type Name = Record<string, string>
type Price = Record<string, number>

type Output = {
  sale?: boolean;
  currencies: string[]; // currency ids
  languages: string[]; // languages ids
  productNames: Name[];
  productPrices: Price[]
}

const outputExample: Output = {
  sale: false,
  currencies: ['USD', 'EUR'],
  languages: ['en', 'ru'],
  productNames: [
    { 'en': 'Macbook Pro 14"'},
    { 'ru': 'Макбук Про 14"'},
  ],
  productPrices: [
    { 'USD': 10.5 },
    { 'EUR': 5 },
  ]
}
