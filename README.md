# Neotech Development
### Тестовое задание

1. Развернуть проект. Стек: `vue3`, `typescript`, `pinia`. Для удобства лучше подключить 
библиотеку компонентов [element-plus](https://element-plus.org/en-US/).
2. Сверстать форму приблизительно как на скриншотах в папке `screenshots`:
   - форма имеет 2 экрана.
   - между экранами можно переключаться и изменять состояние элементов
   - форма имеет обязательный поля отмеченные звездочкой
   - если поле не заполнено (некорректно), нельзя перейти на след экран или сделать сабмит
   - если есть ошибки валидации они должны показываться пользователю
   - после исправления, ошибки валидации дожны пропадать при потере фокуса элемента
   
3. Можно использовать встроенную валидацию компонента element-plus, либо
   можно использовать любую библиотеку валидации, например [async-validator](https://github.com/yiminghe/async-validator).
5. API. Данные для формы нужно получить с сервера. Endpoints: 
   - languages, GET https://raw.githubusercontent.com/maxmarinich/api/master/formik/languages.json
   - currencies, GET https://raw.githubusercontent.com/maxmarinich/api/master/formik/currencies.json
6. В данных `api` есть `primary` проперти. При попытке удалить такое поле из элементв 
   формы нужно показывать алерт с ошибкой либо убрать иконку крестика с элемента. Такое поле нельзя 
   удалить и оно является преселектед значением (`initialValue`).
7. Получив данные с API, мы сохраняем данные в стор. A затем используем их в форме.

Первый экран формы:
1. Переключатель Sale% `default: false`
2. Multi Select Currencies (`required`). Можно выбрать несколько значений, нельзя удалить преселектед(`primary`) value.
   _Символьный поиск можно не реализовывать.* (Задание по жеоланию)_
3. Multi Select Languages (`required`). Можно выбрать несколько значений, нельзя удалить преселектед(`primary`) value.
   _Символьный поиск можно не реализовывать.* (Задание по жеоланию)_
4. Динамическое поле Product Name (`required`). Название для каждого выбранного языка.
   Добавляется/удаляется в зависимости от количества выбранных
   Languages. Соответствует количеству выбранных Languages.


Второй экран формы:
1. Динамическая группа инпутов Product Price (`required`). Количество инпутов соответствует количеству выбранных
   Currencies на первом экране формы. Eсли вернуться со второго экрана на первый и
   изменить поле `currencies`, то в соответствии с этим добавяться/удаляться инпуты для Product Price. 
2. По клику на кнопку `Submit` форма валидируется, если нет ошибок -- в консоль должен попасть вывод описанный в примере `index.ts`
